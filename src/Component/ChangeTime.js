import React from 'react'
import { StyleSheet, View } from 'react-native'
import { CircleButton } from './CircleButton'

const ChangeTime = ({onchange}) => {
    return (
        <>
            <View style={styles.buttnView}>
                <CircleButton title={'10'} onPress={() => onchange(10)} textStyle={{ fontSize: 20 }} />
            </View>
            <View style={styles.buttnView}>
                <CircleButton title={'10'} onPress={() => onchange(15)} textStyle={{ fontSize: 20 }} />
            </View>
            <View style={styles.buttnView}>
                <CircleButton title={'10'} onPress={() => onchange(20)} textStyle={{ fontSize: 20 }} />
            </View>
        </>
    )
}

export default ChangeTime;
const styles = StyleSheet.create({
    buttnView: {
        flex: 1,
        alignItems: 'center'
    }
});