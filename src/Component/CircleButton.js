import React from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { theme } from '../utils/theme'

export const CircleButton = ({
    style = {},
    textStyle = {},
    size = 125,
    title,
    onPress,
    props
}) => {
    return (
        <TouchableOpacity style={[styles.radius,style]} onPress={onPress}>
            <Text style={[styles.text,textStyle]}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles=StyleSheet.create({
    radius:{
        borderRadius:60/2,
        width:60,
        height:60,
        alignItem:'center',
        justifyContent:'center',
        borderColor:theme.colors.blue,
        borderWidth:2
    },
    text:{color:theme.colors.black,fontSize:125/3,alignSelf:'center' }
})
//   default CircleButton;