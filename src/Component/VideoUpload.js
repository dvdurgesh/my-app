// import React, { useEffect, useRef, useState } from "react";
// import {
// 	View,
// 	Text,
// 	StyleSheet,
// 	Image,
// 	ScrollView,
// 	TouchableOpacity,
// } from "react-native";
// import * as ImagePicker from "expo-image-picker";
// import { theme } from "../../../utils/theme";
// import { FontAwesome } from "@expo/vector-icons";
// import Button from "./Button";
// import { Camera } from "expo-camera";
// import { Video } from "expo-av";
// // import Video from "react-native-video";


// let image = ""
// const VideoUpload = ({
// 	preImages,
// 	onImagesUpdate,
// 	style = null,
// 	isbase64 = true,
// 	multiple = false,
// 	captureButtonTitle = "Capture New",
// 	uploadButtonTitle = "Choose a File",
// 	uploadEnable = false,
// 	disabled = false,
// 	captureNewImage = false,
// 	CaptureImageBoxView = true,
// 	maxLimit = 10,
// 	title,
// 	setValue
// }) => {
// 	const video = useRef(null);
// 	const [status, setStatus] = useState({});
// 	const [videos, updateVideos] = useState([]);
// 	useEffect(() => {
// 		if (preImages) {
// 			updateVideos([...videos, ...preImages]);
// 		}
// 	}, []);

// 	// Remove if we get null values===
// 	useEffect(() => {
// 		if (preImages === null) {
// 			updateVideos([]);
// 		}
// 	}, [preImages]);

// 	useEffect(() => {
// 		if (captureNewImage) {
// 			captureImage();
// 		}
// 	}, [captureNewImage]);

// 	const getPermissionAsync = async () => {
// 		// if (Constants.platform.ios) {
// 		const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
// 		if (status !== "granted") {
// 			alert("Sorry, we need camera roll permissions to make this work!");
// 		}
// 		// }
// 	};

// 	const deleteImage = (index) => {
// 		let uImages = videos.splice(0);
// 		uImages.splice(index, 1);
// 		updateVideos(uImages);
// 		onImagesUpdate(uImages.length === 0 ? null : uImages);
// 	};

// 	const selectImage = async () => {

// 		let result
// 		try {

// 			result = await ImagePicker.launchImageLibraryAsync({
// 				mediaTypes: 'Videos',
// 				// mediaType: ImagePicker.MediaTypeOptions.Videos,
// 				// allowsEditing: false,
// 				videoQuality: 'low',
// 				// type:'video'

// 				// allowsEditing: true,
// 				aspect: [4, 3]
// 				// allowsEditing: true,
// 				// aspect: [4, 3],
// 				// quality: 0.2,
// 				// storageOptions:{
// 				// 	skipBackup:true,
// 				// 	path:'images'
// 				//   }
// 			})
// 		} catch (error) {
// 			console.log("selectImage++", error)
// 		}
// 		console.log("selectImage========>2===", result)

// 		if (
// 			result?.assets?.[0]?.uri.split(".").filter((v) => v === "mp4")?.[0] === "mp4"
// 		) {
// 			console.log("ddk===========1122==>", result)
// 			console.log("ddk========>", videos)

// 			// const base64Img = await FileSystem.readAsStringAsync(result.uri, {
// 			// 	encoding: FileSystem.EncodingType.Base64,
// 			// });

// 			let updatedImages = [...videos, result?.assets];
// 			if (multiple) {
// 				onImagesUpdate(updatedImages);
// 			} else {
// 				onImagesUpdate(updatedImages[0]);
// 			}
// 			updateVideos(updatedImages);
// 			setValue(true)
// 		} else if (result?.assets === null) {

// 		}
// 		else {
// 			alert("Only Video files are allowed")
// 		}
// 	};

// 	const captureImage = async () => {
// 		// if (Constants.platform.android) {
// 		const { status } = await Camera.requestCameraPermissionsAsync();
// 		if (status != "granted") {
// 			Alert.alert("please grant permission in setting and Try");
// 		}
// 		// }

// 		let result = await ImagePicker.launchCameraAsync({
// 			mediaTypes: ImagePicker.MediaTypeOptions.Videos,
// 			allowsEditing: false,
// 			videoQuality: 'low',
// 			// durationLimit:20000,

// 			aspect: [4, 3],
// 			// quality: 0.2,
// 		});
// 		if (result?.assets?.[0]?.uri.split(".").filter((v) => v === "jpeg")?.[0] === "jpeg" ||
// 			result?.assets?.[0]?.uri.split(".").filter((v) => v === "jpg")?.[0] === "jpg" ||
// 			result?.assets?.[0]?.uri.split(".").filter((v) => v === "png")?.[0] === "png" ||
// 			result?.assets?.[0]?.uri.split(".").filter((v) => v === "mp4")?.[0] === "mp4"
// 		) {
// 			console.log("video data ===>", result)
// 			// console.log("ddk========>",videos)
// 			// const base64Img = await FileSystem.readAsStringAsync(result.uri, {
// 			// 	encoding: FileSystem.EncodingType.Base64,
// 			// });
// 			let updatedImages = [...videos, result?.assets];
// 			if (multiple) {
// 				onImagesUpdate(updatedImages);
// 			} else {
// 				onImagesUpdate(updatedImages[0]);
// 			}
// 			updateVideos(updatedImages);
// 			setValue(true)
// 		} else if (result?.canceled === true) {
// 			console.log("result", result)
// 		}
// 		else {
// 			alert("Only JPEG, JPG, and PNG files are allowed")
// 		}

// 	};
// 	console.log("Video details=====>", videos)
// 	return (
// 		<View style={[{ ...styles.uploadContainer }, style]}>
// 			{videos.length < 1 && CaptureImageBoxView ? (
// 				<View
// 					style={[
// 						{
// 							alignItems: "center",
// 							justifyContent: "center",
// 							padding: 30,
// 							// borderStyle: 'dotted',
// 							borderWidth: 1,
// 							borderRadius: 10,
// 							borderColor: theme.colors.primary,
// 						},
// 						style,
// 					]}
// 				>
// 					<Image source={require("../../../../assets/upload.png")} />
// 					<Text style={styles.uploadText}>Upload Video</Text>
// 					<View style={{ flexDirection: "row" }}>
// 						{!disabled && (
// 							<Button
// 								title={title ? title : "Take Video"}
// 								textStyle={{ fontSize: theme.typography.body2.s }}
// 								style={{ marginTop: 10 }}
// 								onPress={() => captureImage()}
// 							/>
// 						)}
// 					</View>
// 					{uploadEnable && !disabled && (
// 						<View style={{ flexDirection: "row" }}>
// 							<Button
// 								title={"Upload Video"}
// 								textStyle={{ fontSize: theme.typography.body2.s }}
// 								style={{ marginTop: 10 }}
// 								onPress={() => selectImage()}
// 							/>
// 						</View>
// 					)}
// 				</View>
// 			) : !multiple ? (
// 				<View>
// 					{console.log("dataa====>", videos)}
// 					{videos && (

// 						<Video
// 							ref={video}
// 							style={styles.video}
// 							source={{
// 								// uri: 'https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
// 								uri: videos?.[0]?.[0]?.uri,
// 							}}
// 							useNativeControls
// 							resizeMode="contain"
// 							isLooping
// 							onPlaybackStatusUpdate={status => setStatus(() => status)}
// 						/>
// 					)}
// 					{!disabled &&
// 						<TouchableOpacity
// 							onPress={() => deleteImage(0)}
// 							style={styles.crossBtnView}
// 						>
// 							<Image
// 								source={require("../../../../assets/icons/remove.png")}
// 								style={{ height: 14, width: 14, padding: 5 }}
// 							/>
							 
// 						</TouchableOpacity>
// 					}
// 				</View>
// 			) : (
// 				<ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
// 					<View style={styles.imagePreview}>
// 						{videos &&
// 							videos.map((image, index) => {
// 								console.log("fuction  ===== >  11=====>", image)
// 								return (
// 									<View key={index}>
// 										<View
// 											style={styles.eachImgView}
// 										>
// 											{image && (

												 
// 												<Video
// 													ref={video}
// 													style={styles.video}
// 													source={{
// 														// uri: 'https://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
// 													uri: image?.uri,
// 													}}
// 													useNativeControls
// 													resizeMode="contain"
// 													isLooping
// 													onPlaybackStatusUpdate={status => setStatus(() => status)}
// 												/>
// 											)}
// 											{
// 												!disabled &&
// 												<TouchableOpacity
// 													onPress={() => deleteImage(index)}
// 													style={styles.crossBtnView}
// 												>
// 													<Image
// 														source={require("../../../../assets/icons/remove.png")}
// 														style={{ height: 14, width: 14, padding: 5 }}
// 													/>
// 												</TouchableOpacity>
// 											}
// 										</View>
// 									</View>
// 								);
// 							})}
// 						<View>
// 							{!disabled && videos !== "" && videos.length < maxLimit && (
// 								<View>
// 									<View style={styles.imageView1}>
// 										<View>
// 											<TouchableOpacity
// 												onPress={() =>
// 													uploadEnable ? selectImage() : captureImage()
// 												}
// 												style={{ flexDirection: "row" }}
// 											>
// 												{/* <Image
//                       source={require('../../../../assets/icons/upload.png')}
//                     /> */}
// 												<View style={styles.addView}>
// 													<FontAwesome
// 														name="plus"
// 														size={13}
// 														style={{
// 															paddingHorizontal: 10,
// 															color: theme.colors.primary,
// 														}}
// 													/>
// 												</View>
// 												<Text style={styles.uploadText}>Take Video</Text>
// 											</TouchableOpacity>
// 										</View>
// 									</View>

// 									{true ? <View style={[styles.imageView1, { marginTop: 5 }]}>
// 										<View>
// 											<TouchableOpacity
// 												onPress={() =>
// 													selectImage()
// 												}
// 												style={{ flexDirection: "row" }}
// 											>
// 												{/* <Image
//                       source={require('../../../../assets/icons/upload.png')}
//                     /> */}
// 												<View style={styles.addView}>
// 													<FontAwesome
// 														name="plus"
// 														size={13}
// 														style={{
// 															paddingHorizontal: 10,
// 															color: theme.colors.primary,
// 														}}
// 													/>
// 												</View>
// 												<Text style={styles.uploadText}>Upload Video</Text>
// 											</TouchableOpacity>
// 										</View>
// 									</View>
// 										:
// 										null}

// 								</View>
// 							)}
// 						</View>
// 					</View>
// 				</ScrollView>
// 			)}
// 		</View>
// 	);
// };

// export default VideoUpload;

// const styles = StyleSheet.create({
// 	uploadContainer: {
// 		// borderStyle: 'dotted',
// 		// borderWidth: 1,
// 		// borderRadius: 10,
// 		// borderColor: theme.colors.primary,
// 		backgroundColor: theme.colors.white,
// 	},
// 	uploadText: {
// 		fontSize: theme.typography.subTitle.fontSize,
// 		color: theme.colors.white,
// 		fontWeight: "600",
// 		// marginTop: 10,
// 	},
// 	imagePreview: {
// 		width: "100%",
// 		flexDirection: "row",
// 		marginVertical: 10,
// 		alignItems: "center",
// 	},
// 	eachImgView: {
// 		position: "relative",
// 		flex: 1,
// 		marginRight: 10,
// 		flexDirection: "row",
// 		alignItems: "center",
// 	},
// 	imageView: {
// 		overflow: "hidden",
// 		borderRadius: 10,
// 		width: 100,
// 		height: 100,
// 	},
// 	fullImageView: {
// 		// overflow: "hidden",
// 		borderRadius: 10,
// 		width: "100%",
// 		height: 300,
// 		resizeMode: "cover",
// 	},
// 	imageView1: {
// 		// width: 100,
// 		// height: 50,
// 		overflow: "hidden",
// 		borderRadius: 10,
// 		flexDirection: "row",
// 		paddingHorizontal: 20,
// 		paddingVertical: 10,
// 		borderWidth: 1,
// 		alignItems: "center",
// 		justifyContent: "center",
// 		borderColor: theme.colors.primary,
// 		backgroundColor: theme.colors.primary,
// 	},
// 	crossBtnView: {
// 		position: "absolute",
// 		right: -7,
// 		top: -8,
// 		alignItems: "center",
// 		justifyContent: "center",
// 		backgroundColor: theme.colors.lightGray,
// 		borderRadius: 10,
// 		width: 20,
// 		height: 20,
// 		borderColor: theme.colors.primary,
// 		borderWidth: 1,
// 		padding: 10,
// 	},
// 	addView: {
// 		backgroundColor: theme.colors.white,
// 		alignItems: "center",
// 		justifyContent: "center",
// 		height: 20,
// 		width: 30,
// 		borderRadius: 3,
// 		right: 10,
// 	},
// 	video: {
// 		// alignSelf: 'center',
// 		resizeMode: "cover",

// 		width: "100%",
// 		height: 300,
// 	},
// });
