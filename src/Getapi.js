import axios from 'axios';
import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, StyleSheet, Image } from 'react-native'
import { TouchableOpacity } from 'react-native'
import { Modal } from 'react-native'
import { TextInput } from 'react-native'
import { ActivityIndicator } from 'react-native';
import { CircleButton } from './Component/CircleButton';
// import ImageUpload from './Component/ImageUpload';
// import VideoUpload from './Component/VideoUpload';
  
const Getapi = () => {
    const [value, setValue] = useState([])
    const [values, setValues] = useState()
    const [add, setAdd] = useState(false);
    const [names, setName] = useState("");
    const [emails, setEmail] = useState("");
    const [avatars, setAvatar] = useState("");
    const [loading, setLoading] = useState(true);
    const [modelloading, setModelLoading] = useState(true);
const [updateModel,setUpdateModel]=useState(false);
const [updates,setUpdates]=useState(false);
const [deletemodal,setDeletemodal]=useState(false);
const [deleteid,setDeleteid]=useState('');
    const [models, setModels] = useState(false);
    useEffect(() => {
        getUser()
    }, []);
    const getUser = () => {
        setLoading(true);
        axios.get('http://localhost:5000/user')
            .then(function (res) {
                setValue(res?.data)
                setLoading(false);
            })
            .catch((error) => { console.log(error); });
    }



    const addUser = () => {
        setAdd(true);
    }
     
    console.log("Data==112", value);
    const detail = (data) => {
        // setValues(data);
        const body = data._id;
        axios.get(`http://localhost:5000/user/` + body)
            .then((res) => {
                setValues(res?.data)
                setModels(true);
                console.log("user", values);
            })
    }

    const submits = () => {
        const data = { name: names, email: emails, avatar: avatars }
        axios.post("http://localhost:5000/user/", data)
            .then(res => {
                setAdd(false);
                console.log("successfully submit")
                getUser();
                 
            })
    }

    const deletes = () => {
        axios.delete(`http://localhost:5000/user/` + updates)
            .then((res) => {
                setDeletemodal(false);
                getUser();
                setUpdates('');

                console.log("user", values);
            })

    }
 
    const patched = () => {
        
        const data = { name: names, avatar: avatars }
        axios.patch(`http://localhost:5000/user/`+updates, data)
            .then(res => {
                setUpdateModel(false);
                console.log("successfully submit")
                getUser();
                setEmail('');
                setName('');
                setAvatar('');
                setUpdates('');

            })
    }
     // console.log("dataa==", values)
     const [photo, updateImages] = useState('');
    return (
        <>
            <View style={{ paddingHorizontal: 20, paddingVertical: 40 }}>
             
            {/* <VideoUpload
            uploadEnable={true}
            onImagesUpdate={(images) => updateImages(images)}
            preImages={photo}
            style={{ padding: 5 }}
          /> */}
                <View style={{ flexDirection: 'row' }}>
                  
                    <Text>Data</Text>

                    <TouchableOpacity onPress={() => addUser()}>

                        <Text style={styles.buttons}>Add user</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={value}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {
                        // console.log("gre",item)
                        return (
                            <>
                                {loading ? <ActivityIndicator /> :
                                    <View style={{ marginVertical: 5, backgroundColor: "yellow", padding: 10, borderRadius: 10 }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <TouchableOpacity onPress={() => detail(item)}>
                                                <Text style={{ paddingRight: 10 }}>{item?.name}</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => {
                                                setDeletemodal(true);
                                                setUpdates(item._id);
                                            }} >
                                                <Text style={styles.buttons}>Delete</Text></TouchableOpacity>
                                                <TouchableOpacity onPress={() =>{ 
                                                  setUpdateModel(true);  
                                                    setUpdates(item._id);
                                                }} >
                                                <Text style={styles.buttons}>Update</Text></TouchableOpacity>

                                        </View>
                                        <Text>{item?.email}</Text>
                                        <Text>{item?.avatar}</Text>
                                    </View>
                                }
                            </>
                        )
                    }} />
            <CircleButton title={"+"} />

            </View>
            <Modal
                animationType='slide'
                transparent={true}
                visible={add}
                onRequestClose={() => setAdd(false)}
            >
                <View style={{ justifyContent: 'center', alignItems: 'center', top: 200, marginHorizontal: 20, height: 300, backgroundColor: "#fff", borderRadius: 10, borderWidth: 1 }}>

                    <Text>name</Text>
                    <View>
                        <TextInput
                            style={styles.input}
                            onChangeText={setName}
                            value={names}
                            placeholder="useless placeholder"
                        // keyboardType="numeric"
                        />
                        <Text>email</Text>

                        <TextInput
                            style={styles.input}
                            onChangeText={setEmail}
                            value={emails}
                            placeholder="useless placeholder"
                        // keyboardType="numeric"
                        />
                        <Text>avatar</Text>

                        <TextInput
                            style={styles.input}
                            onChangeText={setAvatar}
                            value={avatars}
                            placeholder="useless placeholder"
                        // keyboardType="numeric"
                        />

                    </View>
                    <TouchableOpacity onPress={() => submits()}><Text style={styles.buttons}>Submit</Text></TouchableOpacity>
                    <Text style={{ paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'red', marginTop: 10, borderRadius: 5 }} onPress={() => setAdd(false)}>Close</Text>
                </View>

            </Modal>
            <Modal
                animationType='slide'
                transparent={true}
                visible={models}
                onRequestClose={() =>
                    setModels(false)}
            >
                <View style={{ justifyContent: 'center', alignItems: 'center', top: 200, marginHorizontal: 20, height: 300, backgroundColor: "#fff", borderRadius: 10, borderWidth: 1 }}>
                    {/* <View style={{ paddingRight: 5 }}>
                        <Image source={{ uri: userData.image }}
                            style={{ width: 77, height: 82, borderRadius: 5, }}
                        /></View> */}
                    <Text style={{ width: 300 }}>{values?.email}</Text>
                    <Text style={{ paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'red', marginTop: 10, borderRadius: 5 }} onPress={() => setModels(false)}>Close</Text>
                </View>
            </Modal>
            <Modal
                animationType='slide'
                transparent={true}
                visible={updateModel}
                onRequestClose={() => setUpdateModel(false)}
            >
                <View style={{ justifyContent: 'center', alignItems: 'center', top: 200, marginHorizontal: 20, height: 300, backgroundColor: "#fff", borderRadius: 10, borderWidth: 1 }}>

                    <Text>name</Text>
                    <View>
                        <TextInput
                            style={styles.input}
                            onChangeText={setName}
                            value={names}
                            placeholder="useless placeholder"
                        // keyboardType="numeric"
                        />
                        {/* <Text>email</Text>

                        <TextInput
                            style={styles.input}
                            onChangeText={setEmail}
                            value={emails}
                            placeholder="useless placeholder"
                        // keyboardType="numeric"
                        /> */}
                        <Text>avatar</Text>

                        <TextInput
                            style={styles.input}
                            onChangeText={setAvatar}
                            value={avatars}
                            placeholder="useless placeholder"
                        // keyboardType="numeric"
                        />

                    </View>
                    <TouchableOpacity onPress={() => patched()}><Text style={styles.buttons}>Submit</Text></TouchableOpacity>
                    <Text style={{ paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'red', marginTop: 10, borderRadius: 5 }} onPress={() => setUpdateModel(false)}>Close</Text>
                </View>

            </Modal>
            <Modal
                animationType='slide'
                transparent={true}
                visible={deletemodal}
                onRequestClose={() => setDeletemodal(false)}
            >
                <View style={{ justifyContent: 'center', alignItems: 'center', top: 200, marginHorizontal: 20, height: 300, backgroundColor: "#fff", borderRadius: 10, borderWidth: 1 }}>
 
                    <TouchableOpacity onPress={() => deletes()}><Text style={styles.buttons}>Delete</Text></TouchableOpacity>
                    <Text style={{ paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'red', marginTop: 10, borderRadius: 5 }} onPress={() => setDeletemodal(false)}>Cancel</Text>
                </View>

            </Modal>
        </>
    )
}

export default Getapi
const styles = StyleSheet.create({
    input: {
        padding: 10,
        borderWidth: 1,
        borderRadius: 10,
    },
    buttons: {
        padding: 10, borderWidth: 1, borderRadius: 10, marginLeft: 10
    }

});