

const colors = {
    primary: '#2B2926',
    secondary: '#707070',
    blue: '#2680EB',
    skyBlue: '#4FCAEC',
    lightBlue: '#7AB2FB',
    green: '#72DA9B',
    lime: '#E1E000',
    orange: '#FF2828',
    darkGray: '#524D4D',
    gray: '#333333',
    lightGray: '#e3e2dd',
    lightGray1: '#ECECEC',
    textColor: '#787878',
    white: '#fff',
    tabBg: '#F1F1F1',
    failBG: '#F5F5F5',
    red: '#FF0000',
    profileTitle: '#808080',
    black: '#000000'
};


export const theme = {
    colors,

};