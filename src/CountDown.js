import React, { useEffect, useRef, useState } from 'react'
import { Text, View } from 'react-native'

const minToMS = (min) => min * 1000 * 60
const formatTime = (time) => (time < 10 ? `0${time}` : time)
const CountDown = ({ minutes = 0.1, isPause, onEnd, onProgress }) => {
    const interval = useRef(null);

    const [millis, setMillis] = useState(null);

    const reset =()=>setMillis(minToMS(minutes))

    const countDown = () => {
        setMillis((time) => {
            if (time === 0) {
                clearInterval(interval.current);
                onEnd(reset);
                return time;
            }
            const timeLeft = time - 1000
            return timeLeft;
        })
    };


    useEffect(() => {
        setMillis(minToMS(minutes));
    }, [minutes]);

    useEffect(() => {
        onProgress(millis / minToMS(minutes));
    }, [millis]);

    useEffect(() => {
        if (isPause) {
            if (interval.current) clearInterval(interval.current);
            return;
        }
        interval.current = setInterval(countDown, 1000)
        return () => clearInterval(interval.current);
    }, [isPause]);

    const minute = Math.floor(millis / 1000 / 60) % 60;
    const second = Math.floor(millis / 1000) % 60;
    return (
        <View>
            <Text style={{alignSelf:'center',fontSize:30}}>
                {formatTime(minute)}:{formatTime(second)}
            </Text>
        </View>
    )
}

export default CountDown