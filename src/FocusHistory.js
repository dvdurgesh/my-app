import React from 'react'
import { FlatList, Text, View } from 'react-native';

const FocusHistory = ({history}) => {
    if (!history || !history.length) return null;
const renderItem=({item})=> <Text>{item}</Text>

  return (
    <View>
        <Text>Focus Text == Data</Text>
        <FlatList
        data={history}
        renderItem={renderItem}
        />
    </View> 
  )
}

export default FocusHistory