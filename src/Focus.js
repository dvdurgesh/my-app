import React, { useState } from 'react'
import { Text, TextInput, TouchableOpacity, View } from 'react-native'

const Focus = ({add}) => {
    const [currentStatus, setCurrentStatus] = useState(null)
    const [changeText, onChangeText] = useState("")
    return (
        <View>
            <TextInput
                editable
                // multiline
                numberOfLines={4}
                maxLength={40}
                onChangeText={text => onChangeText(text)}
                // value={value}
                style={{ borderWidth:1, borderRadius:20, padding: 10 }}
            />
<TouchableOpacity style={{borderWidth:1, borderRadius:20,width:120,margin:10,backgroundColor:"gray"}} onPress={()=>add(changeText)}>
    <Text style={{paddingVertical:10,paddingHorizontal:20}}>Submit Data</Text>
    </TouchableOpacity>
        </View>
    )
}

export default Focus