import { useState } from "react";
import { StyleSheet, Text, Vibration } from "react-native"
import { View } from "react-native"
import { useKeepAwake } from 'expo-keep-awake';
import { ProgressBar } from "react-native-paper";
import ChangeTime from "./Component/ChangeTime";
import { CircleButton } from "./Component/CircleButton";
import CountDown from "./CountDown";

const ONE_SECOND_IN_MS = 1000;

const PATTERN = [
    1 * ONE_SECOND_IN_MS,
    1 * ONE_SECOND_IN_MS,
    1 * ONE_SECOND_IN_MS,
    1 * ONE_SECOND_IN_MS,
    1 * ONE_SECOND_IN_MS,
    1 * ONE_SECOND_IN_MS,
];

export const Timer = ({ focusSubject, clearSubject,onTimerEnd }) => {
    useKeepAwake();
    const [starts, setStarts] = useState(false);
    const [progress, setProgess] = useState(1);
    const [minutes, setMinuts] = useState(0.1);

    const onEnd = (reset) => {
        Vibration.vibrate(PATTERN)
        setProgess(1)
        setStarts(false)
        reset();
        onTimerEnd(focusSubject);
    }
    return (
        <View style={styles.container}>
            <View style={styles.timer}>
                <CountDown
                    minutes={minutes}
                    isPause={!starts}
                    onProgress={setProgess}
                    onEnd={onEnd}
                />
            </View>
            <View style={{ alignItems: 'center', justifyContent: 'center', height: 150 }}>
                <Text>Focus on</Text>
                <Text>{focusSubject}</Text>
            </View>
            <View>
                <ProgressBar
                    progress={progress}
                    color={"red"}
                    style={{ height: 7, marginBottom: 50 }}
                />
            </View>
            <View style={styles.btnView}>
                <ChangeTime onchange={setMinuts} />
            </View>
            <View style={styles.btnView}>
                {!starts &&
                    <CircleButton title={"Start"} onPress={() => setStarts(true)} textStyle={{ fontSize: 20 }} />
                }
                {starts &&
                    <CircleButton title={"Pause"} onPress={() => setStarts(false)} textStyle={{ fontSize: 20 }} />
                }
            </View>
            <View style={styles.btnView}>
                <CircleButton onPress={clearSubject} title={'Clear'} textStyle={{ fontSize: 20 }} />
            </View>

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    timer: {
        flex: 0.3,
        alignContent: 'center',
        justifyContent: 'center',
    },
    btnView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    }
});
