import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Focus from './src/Focus';
import FocusHistory from './src/FocusHistory';
// import Video from 'react-native-video';

import Getapi from './src/Getapi';
// import GetwithParams from './src/GetwithParams';
import Details from './src/PostApi';
import { Timer } from './src/Timer';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Pressable } from 'react-native';

export default function App() {
  
  const [history, setHistory] = useState(["gg"])
  const statetext=[
    {buttom:"red",text:"rows Data"},
    {buttom:"red",text:"rows Data"},
    {buttom:"red",text:"rows Data"},
  ]
  return (
    <View style={styles.container}>
      {/* {!status ?
        <>
          <Focus add={setStatus} />
          <FocusHistory history={history} />
        </>
        :
        // <Text>Open up App.js to start working on your app!</Text>
        <Timer
          focusSubject={status}
          onTimerEnd={(subject) => setHistory([...history,subject])}
          clearSubject={() => setStatus(null)}
        />

      } */}
      {/* <TouchableOpacity   */}
      
{statetext.map((e,index)=>{
  const [status, setStatus] = useState(null)
  const [statustext, setStatustext] = useState(false)

  return(

<View key={index}>
       <Pressable  
      // accessibilityRole="button"
      onPress={()=>{setStatustext(!statustext)}}
      onHoverIn={()=>{setStatus(true)}}
      onHoverOut={()=>setStatus(false)}
         > 
        <View style={{padding:20,backgroundColor:status? 'green':'red',margin:10,width:500,borderRadius:20,alignItems:'center'}}>  
      <Text>{e.buttom + (index+1)}</Text>
      </View>
      </Pressable>
      {statustext &&
      <Text>{e.text}</Text>
      }
      </View>
    )
   })}
   {/* </TouchableOpacity> */}
      {/* <Getapi /> */}

      {/* <GetwithParams /> */}
      {/* <Details /> */}
    </View>
   
    );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
    padding: 20
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});

// import React, { useEffect } from 'react';
// import { View, Button } from 'react-native';
// import crashlytics from '@react-native-firebase/crashlytics';

// async function onSignIn(user) {
//   crashlytics().log('User signed in.');
//   await Promise.all([
//     crashlytics().setUserId(user.uid),
//     crashlytics().setAttribute('credits', String(user.credits)),
//     crashlytics().setAttributes({
//       role: 'admin',
//       followers: '13',
//       email: user.email,
//       username: user.username,
//     }),
//   ]);
// }

// export default function App() {
//   useEffect(() => {
//     crashlytics().log('App mounted.');
//   }, []);

//   return (
//     <View>
//       <Button
//         title="Sign In"
//         onPress={() =>
//           onSignIn({
//             uid: 'Aa0Bb1Cc2Dd3Ee4Ff5Gg6Hh7Ii8Jj9',
//             username: 'Joaquin Phoenix',
//             email: 'phoenix@example.com',
//             credits: 42,
//           })
//         }
//       />
//       <Button title="Test Crash" onPress={() => crashlytics().crash()} />
//     </View>
//   );
// }